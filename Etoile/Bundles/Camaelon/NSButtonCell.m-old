#include <Foundation/Foundation.h>
#include <AppKit/AppKit.h>
#include "GraphicToolbox.h"
#include "GNUstepGUI/GSDrawFunctions.h"

@implementation GSDrawFunctions (theme)
+ (void) drawKnobButton: (NSRect) border inView: (NSView*) view highlighted: (BOOL) highlighted
{
	[GSDrawFunctions drawButton: border inView: view highlighted: highlighted];
}

+ (void) drawGradient: (NSData*) gradient withSize: (NSArray*) size 
	border: (NSRect) border clip: (NSBezierPath*) clip 
{
        NSAffineTransform *transform;
	NSDictionary* shader;

	transform=[[NSAffineTransform alloc] init];
	transform->matrix.m11=border.size.width;
	transform->matrix.m12=0.0;
	transform->matrix.m21=0.0;
	transform->matrix.m22=border.size.height;
	transform->matrix.tX=border.origin.x;
	transform->matrix.tY=border.origin.y;

	shader = [[NSDictionary alloc] initWithObjectsAndKeys:
			[NSNumber numberWithInt: 1], @"ShadingType",
			[NSArray arrayWithObjects:
				[NSNumber numberWithFloat: 0.0],
				[NSNumber numberWithFloat: 1.0],
				[NSNumber numberWithFloat: 0.0],
				[NSNumber numberWithFloat: 1.0], nil], @"Domain",
			transform, @"Matrix",
                        [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithInt: 0],@"FunctionType",
                                [NSArray arrayWithObjects:
                                        [NSNumber numberWithFloat: 0.0], // origin.x
                                        [NSNumber numberWithFloat: 1.0], // x + width
                                        [NSNumber numberWithFloat: 0.0], // origin.y
                                        [NSNumber numberWithFloat: 1.0], // y + height
                                        nil],
                                        @"Domain",
                                [NSArray arrayWithObjects:
                                        [NSNumber numberWithFloat: 0.0],
                                        [NSNumber numberWithFloat: 1.0],
                                        [NSNumber numberWithFloat: 0.0],
                                        [NSNumber numberWithFloat: 1.0],
                                        [NSNumber numberWithFloat: 0.0],
                                        [NSNumber numberWithFloat: 1.0],
                                        nil],
                                        @"Range",
                                [NSNumber numberWithInt: 8],@"BitsPerSample",
				size, @"Size",
                                gradient ,@"DataSource",
                                nil], @"Function", 
			nil];

	[clip setClip];
        PSshfill(shader);
	[transform release];
	//[shader release]; //FIXME ..
}

+ (void) drawButton: (NSRect) border inView: (NSView*) view highlighted: (BOOL) highlighted
{
	NSBezierPath* pathFrame = [NSBezierPath bezierPath];
	NSBezierPath* path = [NSBezierPath bezierPath];
	[path appendBezierPathWithRoundedRectangle: border withRadius: 8.0];

	if (highlighted)
	{
		[[NSColor colorWithCalibratedRed: 1.0 green: 1.0 blue: 1.0 alpha: 1.0] set];
	}
	else
	{
		[[NSColor colorWithCalibratedRed: 0.7 green: 0.7 blue: 0.7 alpha: 1.0] set];
	}

	static const unsigned char data[]= {
		180, 180, 180,
		255, 255, 255
	};

	NSData* gradient = [NSData dataWithBytesNoCopy: data length: sizeof(data)];
	NSArray* size = [NSArray arrayWithObjects: [NSNumber numberWithInt: 1], [NSNumber numberWithInt: 2], nil];

	[GSDrawFunctions drawGradient: gradient withSize: size border: border clip: path];
	[pathFrame appendBezierPathWithRoundedRectangle: border withRadius: 8.0];
	[[NSColor colorWithCalibratedRed: 0.0 green: 0.0 blue: 0.0 alpha: 0.4] set];
}

+ (void) drawxButton: (NSRect) border inView: (NSView*) view highlighted: (BOOL) highlighted 
{
	if (highlighted)
	{
		[[NSColor selectedControlColor] set];
		NSRectFill (border);
 		[GSDrawFunctions drawGrayBezel: border : NSZeroRect];
	}
	else
	{
		[[NSColor controlBackgroundColor] set];
		NSRectFill (border);
		[GSDrawFunctions drawButton: border : NSZeroRect];
	}
}

+ (void) xdrawFirstResponderFrame: (NSRect) frame
{
	NSDottedFrameRect(frame);
}
+ (void) drawFirstResponderFrame: (NSRect) cellFrame
{
	NSBezierPath* path = [NSBezierPath bezierPath];
	NSRect rect = NSMakeRect (cellFrame.origin.x + 2, cellFrame.origin.y + 2, cellFrame.size.width - 4, cellFrame.size.height - 4);
	rect = cellFrame;
	[path appendBezierPathWithRoundedRectangle: rect withRadius: 8.0];
	[[NSColor colorWithCalibratedRed: 0.5 green: 0.5 blue: 1.0 alpha: 0.3] set];
	[path setLineWidth: 5];
	[path stroke];
	[[NSColor colorWithCalibratedRed: 0.5 green: 0.5 blue: 1.0 alpha: 0.5] set];
	[path setLineWidth: 3];
	[path stroke];
	[[NSColor colorWithCalibratedRed: 0.4 green: 0.4 blue: 1.0 alpha: 1.0] set];
	[path setLineWidth: 1];
	[path stroke];
}
@end

@implementation NSButtonCell (theme)
- (BOOL) isOpaque {
    return NO;
}

- (void) drawWithFrame: (NSRect)cellFrame inView: (NSView*)controlView
{
  // Save last view drawn to
  if (_control_view != controlView)
    _control_view = controlView;

  // transparent buttons never draw
  if (_buttoncell_is_transparent)
    return;

  // do nothing if cell's frame rect is zero
  if (NSIsEmptyRect(cellFrame))
    return;

  // draw the border if needed
  if ((_cell.is_bordered) &&
      (!_shows_border_only_while_mouse_inside || _mouse_inside))
    {
		BOOL highlighted = NO;

		if (_cell.is_highlighted && (_highlightsByMask & NSPushInCellMask))
		{
			highlighted = YES;
		}
	
		[[self controlView] lockFocus];

		if ([[self controlView] isKindOfClass: [NSScroller class]])
		{
			[GSDrawFunctions drawKnobButton: cellFrame inView: controlView
				highlighted: highlighted];
		}
		else
		{
			[GSDrawFunctions drawButton: cellFrame inView: controlView 
				highlighted: highlighted];
		}
	      
		[[self controlView] unlockFocus];
    }

  [self drawInteriorWithFrame: cellFrame inView: controlView];
}

- (void) OdrawWithFrame: (NSRect)cellFrame inView: (NSView*)controlView {
  NSRect inboxRect;

  // Save last view drawn to
  if (_control_view != controlView)
    _control_view = controlView;

  // transparent buttons never draw
  if (_buttoncell_is_transparent)
    return;

  // do nothing if cell's frame rect is zero
  if (NSIsEmptyRect(cellFrame))
    return;

  // [GraphicToolbox setColor];

  // draw the border if needed
  if (_cell.is_bordered)
    {
      [controlView lockFocus];
      // FIXME Should check the bezel and gradient style
      if (_cell.is_highlighted && (_highlightsByMask & NSPushInCellMask))
        {
      	    if ([[self controlView] isKindOfClass: [NSScroller class]])
       	    {
			NSBezierPath* path = [NSBezierPath bezierPath];
			[path appendBezierPathWithRoundedRectangle: cellFrame withRadius: 8.0];
      	    		[[NSColor colorWithCalibratedRed: 0.9 green: 0.9 blue: 0.9 alpha: 1.0] set];
			[path fill];
            }
	    else
	    {
			NSBezierPath* path = [NSBezierPath bezierPath];
			[path appendBezierPathWithRoundedRectangle: cellFrame withRadius: 8.0];
      	    		[[NSColor colorWithCalibratedRed: 1.0 green: 1.0 blue: 1.0 alpha: 1.0] set];
			[path fill];
	    }
        }
      else
        {
	    //NSLog (@"button, : %@", [self controlView]);
      
      NSColor* startColor = [NSColor colorWithCalibratedRed: 0.8 green: 0.8 blue: 1.0 alpha: 1.0];
      NSColor* endColor = [NSColor colorWithCalibratedRed: 1.0 green: 1.0 blue: 1.0 alpha: 1.0];

	    id controlView = [self controlView];
	    BOOL pushed = NO;
      
	    {
	        if ([controlView isFlipped])
		{
			NSBezierPath* path = [NSBezierPath bezierPath];
			[path appendBezierPathWithRoundedRectangle: cellFrame withRadius: 8.0];
      	    		[[NSColor colorWithCalibratedRed: 0.7 green: 0.7 blue: 0.7 alpha: 1.0] set];
			[path fill];
		}
		else
		{
			NSBezierPath* path = [NSBezierPath bezierPath];
			[path appendBezierPathWithRoundedRectangle: cellFrame withRadius: 8.0];
      	    		[[NSColor colorWithCalibratedRed: 0.7 green: 0.7 blue: 0.7 alpha: 1.0] set];
			[path fill];

//			NSRect rect = NSMakeRect (cellFrame.origin.x + 1, cellFrame.origin.y + 2, cellFrame.size.width - 4, cellFrame.size.height - 4);
			//NSRect rect = NSMakeRect (cadrerect.origin.x + 1, cadrerect.origin.y + 1, cadrerect.size.width -2, cadrerect.size.height - 2);
//			inboxRect = rect;

		}
	      
	      //[GraphicToolbox drawButtonOnRect: cellFrame pushed: pushed width: 1];
	      //NSDrawButton(cellFrame, NSZeroRect);
	    }
        }

	  if (_cell.shows_first_responder
	      && [[controlView window] firstResponder] == controlView)
	    {
		NSBezierPath* path = [NSBezierPath bezierPath];
	      if (_cell.is_bordered || _cell.is_bezeled)
		{
			NSRect rect = NSMakeRect (cellFrame.origin.x + 2, cellFrame.origin.y + 2, cellFrame.size.width - 4, cellFrame.size.height - 4);
			[path appendBezierPathWithRoundedRectangle: rect withRadius: 8.0];
      	    		[[NSColor colorWithCalibratedRed: 0.4 green: 0.4 blue: 0.4 alpha: 1.0] set];
			[path setLineWidth: 2];
			[path stroke];
		}
	      else
		{
/*
			[path appendBezierPathWithRoundedRectangle: titleRect withRadius: 8.0];
      	    		[[NSColor colorWithCalibratedRed: 0.7 green: 1.0 blue: 0.7 alpha: 1.0] set];
			[path stroke];
*/
		}
	    }
	      [controlView unlockFocus];
      }

  //NRO : c'est �a qui chie avec les NSMenuItemCell
  //[self drawInteriorWithFrame: inboxRect inView: controlView];
  [self drawInteriorWithFrame: cellFrame inView: controlView];
}

- (void) drawInteriorWithFrame: (NSRect)cellFrame inView: (NSView*)controlView
{
  unsigned	mask;
  NSImage	*imageToDisplay;
  NSRect	imageRect;
  NSAttributedString	*titleToDisplay;
  NSRect	titleRect;
  NSSize	imageSize = {0, 0};
  NSSize        titleSize = {0, 0};
  NSColor	*backgroundColor = nil;
  BOOL		flippedView = [controlView isFlipped];
  NSCellImagePosition ipos = _cell.image_position;

  // transparent buttons never draw
  if (_buttoncell_is_transparent)
    return;

  _control_view = controlView;

  cellFrame = [self drawingRectForBounds: cellFrame];

  if (_cell.is_highlighted)
    {
      mask = _highlightsByMask;

      if (_cell.state)
	mask &= ~_showAltStateMask;
    }
  else if (_cell.state)
    mask = _showAltStateMask;
  else
    mask = NSNoCellMask;

  /* Pushed in buttons contents are displaced to the bottom right 1px.  */
  if (_cell.is_bordered && (mask & NSPushInCellMask))
    {
      cellFrame = NSOffsetRect(cellFrame, 1., flippedView ? 1. : -1.);
    }

  /* Draw the cell's background color.  
     We draw when there is a border or when highlightsByMask
     is NSChangeBackgroundCellMask or NSChangeGrayCellMask,
     as required by our nextstep-like look and feel.  */
/*
  if (_cell.is_bordered 
      || (_highlightsByMask & NSChangeBackgroundCellMask)
      || (_highlightsByMask & NSChangeGrayCellMask))
    {
	if ([GSDrawFunctions drawButtons])
	{
		BOOL highlighted = NO;

		if (_cell.is_highlighted && (_highlightsByMask & NSPushInCellMask))
		{
			highlighted = YES;
		}
		if ([[self controlView] isKindOfClass: [NSScroller class]])
		{
			[GSDrawFunctions drawKnobButton: cellFrame inView: controlView
				highlighted: highlighted];
		}
		else
		{
			[GSDrawFunctions drawButton: cellFrame inView: controlView 
				highlighted: highlighted];
		}
	}
	else
	{
	      // Determine the background color. 
	      if (mask & (NSChangeGrayCellMask | NSChangeBackgroundCellMask))
		{
		  backgroundColor = [NSColor selectedControlColor];
		}
	      else if (_cell.is_bordered) 
		{
		  backgroundColor = [NSColor controlBackgroundColor];
		}
	      
	      if (backgroundColor != nil) 
		{
		  [backgroundColor set];
		  NSRectFill (cellFrame);
		}
	}
    }
*/

  /*
   * Determine the image and the title that will be
   * displayed. If the NSContentsCellMask is set the
   * image and title are swapped only if state is 1 or
   * if highlighting is set (when a button is pushed it's
   * content is changed to the face of reversed state).
   */
  if (mask & NSContentsCellMask)
    {
      imageToDisplay = _altImage;
      if (!imageToDisplay)
        {
	  imageToDisplay = _cell_image;
	}
      titleToDisplay = [self attributedAlternateTitle];
      if (titleToDisplay == nil || [titleToDisplay length] == 0)
        {
	  titleToDisplay = [self attributedTitle];
	}
    }
  else
    {
      imageToDisplay = _cell_image;
      titleToDisplay = [self attributedTitle];
    }

  if (imageToDisplay)
    {
      imageSize = [imageToDisplay size];
    }

  if (titleToDisplay && (ipos == NSImageAbove || ipos == NSImageBelow))
    {
      titleSize = [titleToDisplay size];
    }

  if (flippedView == YES)
    {
      if (ipos == NSImageAbove)
	{
	  ipos = NSImageBelow;
	}
      else if (ipos == NSImageBelow)
	{
	  ipos = NSImageAbove;
	}
    }
  
  switch (ipos)
    {
      case NSNoImage: 
	imageToDisplay = nil;
	titleRect = cellFrame;
	break;

      case NSImageOnly: 
	titleToDisplay = nil;
	imageRect = cellFrame;
	break;

      case NSImageLeft: 
	imageRect.origin = cellFrame.origin;
	imageRect.size.width = imageSize.width;
	imageRect.size.height = cellFrame.size.height;
	if (_cell.is_bordered || _cell.is_bezeled) 
	  {
	    imageRect.origin.x += 3;
	    imageRect.size.height -= 2;
	    imageRect.origin.y += 1;
	  }
	titleRect = imageRect;
	titleRect.origin.x += imageSize.width + xDist;
	titleRect.size.width = cellFrame.size.width - imageSize.width - xDist;
        titleRect.size.width -= 3;
	break;

      case NSImageRight: 
	imageRect.origin.x = NSMaxX(cellFrame) - imageSize.width;
	imageRect.origin.y = cellFrame.origin.y;
	imageRect.size.width = imageSize.width;
	imageRect.size.height = cellFrame.size.height;
	if (_cell.is_bordered || _cell.is_bezeled) 
	  {
	    imageRect.origin.x -= 3;
	    imageRect.size.height -= 2;
	    imageRect.origin.y += 1;
	  }
	titleRect.origin = cellFrame.origin;
	titleRect.size.width = cellFrame.size.width - imageSize.width - xDist;
	titleRect.size.height = cellFrame.size.height;
	titleRect.origin.x += 3;
	titleRect.size.width -= 3;
	break;

      case NSImageAbove: 
	/*
         * In this case, imageRect is all the space we can allocate
	 * above the text. 
	 * The drawing code below will then center the image in imageRect.
	 */
	titleRect.origin.x = cellFrame.origin.x;
	titleRect.origin.y = cellFrame.origin.y;
	titleRect.size.width = cellFrame.size.width;
	titleRect.size.height = titleSize.height;

	imageRect.origin.x = cellFrame.origin.x;
	imageRect.origin.y = cellFrame.origin.y;
	imageRect.origin.y += titleRect.size.height + yDist;
	imageRect.size.width = cellFrame.size.width;
	imageRect.size.height = cellFrame.size.height;
	imageRect.size.height -= titleSize.height + yDist;

	if (_cell.is_bordered || _cell.is_bezeled) 
	  {
	    imageRect.origin.x += 3;
	    imageRect.size.width -= 6;
	    imageRect.size.height -= 1;
	  }
	titleRect.origin.x += 3;
	titleRect.origin.y += 4;
	titleRect.size.width -= 6;
	break;

      case NSImageBelow: 
	/*
	 * In this case, imageRect is all the space we can allocate
	 * below the text. 
	 * The drawing code below will then center the image in imageRect.
	 */
	titleRect.origin.x = cellFrame.origin.x;
	titleRect.origin.y = cellFrame.origin.y + cellFrame.size.height;
	titleRect.origin.y -= titleSize.height;
	titleRect.size.width = cellFrame.size.width;
	titleRect.size.height = titleSize.height;

	imageRect.origin.x = cellFrame.origin.x;
	imageRect.origin.y = cellFrame.origin.y;
	imageRect.size.width = cellFrame.size.width;
	imageRect.size.height = cellFrame.size.height;
	imageRect.size.height -= titleSize.height + yDist;

	if (_cell.is_bordered || _cell.is_bezeled) 
	  {
	    imageRect.size.width -= 6;
	    imageRect.origin.x   += 3;
	    imageRect.size.height -= 1;
	    imageRect.origin.y    += 1;
	  }
	titleRect.size.width -= 6;
	titleRect.origin.x   += 3;
	titleRect.size.height -= 4;
	break;

      case NSImageOverlaps: 
	titleRect = cellFrame;
	imageRect = cellFrame;
	// TODO: Add distance from border if needed
	break;
    }
  if (imageToDisplay != nil)
    {
      NSSize size;
      NSPoint position;

      size = [imageToDisplay size];
      position.x = MAX(NSMidX(imageRect) - (size.width/2.),0.);
      position.y = MAX(NSMidY(imageRect) - (size.height/2.),0.);
      /*
       * Images are always drawn with their bottom-left corner at the origin
       * so we must adjust the position to take account of a flipped view.
       */
      if (flippedView)
	{
	  position.y += size.height;
	}
      [imageToDisplay compositeToPoint: position operation: NSCompositeSourceOver];
    }
  if (titleToDisplay != nil)
    {
      [self _drawAttributedText: titleToDisplay inFrame: titleRect];
    }

  if (_cell.shows_first_responder
      && [[controlView window] firstResponder] == controlView)
    {
	[GSDrawFunctions drawFirstResponderFrame: cellFrame];
	//NSDottedFrameRect(cellFrame);
    }
}

- (void) OdrawInteriorWithFrame: (NSRect)cellFrame inView: (NSView*)controlView {
  unsigned	mask;
  NSImage	*imageToDisplay;
  NSRect	imageRect;
  NSString	*titleToDisplay;
  NSRect	titleRect;
  NSSize	imageSize = {0, 0};
  NSSize        titleSize = {0, 0};
  NSColor	*backgroundColor = nil;
  BOOL		flippedView = [controlView isFlipped];
  NSCellImagePosition ipos = _cell.image_position;
  BOOL		_verticalGradient = YES;

  int i = 0;
  int colortype = 0;

  // transparent buttons never draw
  if (_buttoncell_is_transparent)
    return;

  _control_view = controlView;

  cellFrame = [self drawingRectForBounds: cellFrame];
  [controlView lockFocus];

  if (_cell.is_highlighted)
    {
      mask = _highlightsByMask;

      if (_cell.state)
	mask &= ~_showAltStateMask;
    }
  else if (_cell.state)
    mask = _showAltStateMask;
  else
    mask = NSNoCellMask;

  /* Pushed in buttons contents are displaced to the bottom right 1px.  */
  if (_cell.is_bordered && (mask & NSPushInCellMask))
    {
      cellFrame = NSOffsetRect(cellFrame, 1., flippedView ? 1. : -1.);
    }

  /* Determine the background color. */
  if (mask & (NSChangeGrayCellMask | NSChangeBackgroundCellMask))
    {
      backgroundColor = [NSColor selectedControlColor];
    }

  if (backgroundColor == nil)
    backgroundColor = [NSColor controlBackgroundColor];

  /* Draw the cell's background color.  We always draw the background,
     as required by our nextstep-like look and feel.  */
  [backgroundColor set];
  //NSRectFill (cellFrame);

  // FIXME
  // This is a hack to get NSScroller display vertical or horizontal
  // gradient "correctly" (as we want)
  if (cellFrame.size.width < cellFrame.size.height) _verticalGradient = NO;

  /*
  if (_verticalGradient)
  {
      int nb_pas = cellFrame.size.height;
    for (i = 0; i < nb_pas; i++)
    {
	    float x0 = cellFrame.origin.x;
	    float x1 = cellFrame.origin.x + cellFrame.size.width;
	    float y0 = cellFrame.origin.y;
	    float y1 = cellFrame.origin.y + cellFrame.size.height;
	    float h = y1 - y0;
	    float pas = h / nb_pas;
	    float ny1 = y0 + i*pas;
	    float ny0 = ny1 + pas;
	    float begin = 0.9;
	    float end = 0.5;
	    float cpas = (end - begin) / nb_pas;
	    float c = begin + i*cpas;

	    [[NSColor colorWithCalibratedRed: c green: c blue: c alpha: 1.0] set]; 
	    [NSBezierPath fillRect: NSMakeRect (x0,ny1,cellFrame.size.width,pas)];
    }
  }
  else
  {
      int nb_pas = cellFrame.size.width;
    for (i = 0; i < nb_pas; i++)
    {
	    float x0 = cellFrame.origin.x;
	    float x1 = cellFrame.origin.x + cellFrame.size.width;
	    float y0 = cellFrame.origin.y;
	    float y1 = cellFrame.origin.y + cellFrame.size.height;
	    float w = x1 - x0;
	    float pas = w / nb_pas;
	    float nx0 = x0 + i*pas;
	    float nx1 = nx0 + pas;
	    float begin = 0.9;
	    float end = 0.5;
	    float cpas = (end - begin) / nb_pas;
	    float c = begin + i*cpas;
	    
	    [[NSColor colorWithCalibratedRed: c green: c blue: c alpha: 1.0] set]; 
	    [NSBezierPath fillRect: NSMakeRect (nx0,y0,pas,y1-y0)];
    }
  }
  */
  

  /*
   * Determine the image and the title that will be
   * displayed. If the NSContentsCellMask is set the
   * image and title are swapped only if state is 1 or
   * if highlighting is set (when a button is pushed it's
   * content is changed to the face of reversed state).
   */
  if (mask & NSContentsCellMask)
    {
      imageToDisplay = _altImage;
      if (!imageToDisplay)
	imageToDisplay = _cell_image;
      titleToDisplay = _altContents;
      if (titleToDisplay == nil || [titleToDisplay isEqual: @""])
        titleToDisplay = _contents;
    }
  else
    {
      imageToDisplay = _cell_image;
      titleToDisplay = _contents;
    }

  if (imageToDisplay)
    {
      /* FIXME - the following is a hack!  Because we don't seem to be
	 getting alpha composing of images right, we use this hack of
	 hard-setting manually the background color of the image to
	 the wanted background color ... this should go away when
	 alpha composing of images works 100%.  */
    //  [imageToDisplay setBackgroundColor: backgroundColor];
      imageSize = [imageToDisplay size];
    }

  if (titleToDisplay && (ipos == NSImageAbove || ipos == NSImageBelow))
    {
      titleSize = [self _sizeText: titleToDisplay];
    }

  if (flippedView == YES)
    {
      if (ipos == NSImageAbove)
	{
	  ipos = NSImageBelow;
	}
      else if (ipos == NSImageBelow)
	{
	  ipos = NSImageAbove;
	}
    }
  
  switch (ipos)
    {
      case NSNoImage: 
	imageToDisplay = nil;
	titleRect = cellFrame;
	break;

      case NSImageOnly: 
	titleToDisplay = nil;
	imageRect = cellFrame;
	break;

      case NSImageLeft: 
	imageRect.origin = cellFrame.origin;
	imageRect.size.width = imageSize.width;
	imageRect.size.height = cellFrame.size.height;
	if (_cell.is_bordered || _cell.is_bezeled) 
	  {
	    imageRect.origin.x += 3;
	    imageRect.size.height -= 2;
	    imageRect.origin.y += 1;
	  }
	titleRect = imageRect;
	titleRect.origin.x += imageSize.width + xDist;
	titleRect.size.width = cellFrame.size.width - imageSize.width - xDist;
	if (_cell.is_bordered || _cell.is_bezeled) 
	  {
	    titleRect.size.width -= 3;
	  }
	break;

      case NSImageRight: 
	imageRect.origin.x = NSMaxX(cellFrame) - imageSize.width;
	imageRect.origin.y = cellFrame.origin.y;
	imageRect.size.width = imageSize.width;
	imageRect.size.height = cellFrame.size.height;
	if (_cell.is_bordered || _cell.is_bezeled) 
	  {
	    imageRect.origin.x -= 3;
	    imageRect.size.height -= 2;
	    imageRect.origin.y += 1;
	  }
	titleRect.origin = cellFrame.origin;
	titleRect.size.width = cellFrame.size.width - imageSize.width - xDist;
	titleRect.size.height = cellFrame.size.height;
	if (_cell.is_bordered || _cell.is_bezeled) 
	  {
	    titleRect.origin.x += 3;
	    titleRect.size.width -= 3;
	  }
	break;

      case NSImageAbove: 
	/*
         * In this case, imageRect is all the space we can allocate
	 * above the text. 
	 * The drawing code below will then center the image in imageRect.
	 */
	titleRect.origin.x = cellFrame.origin.x;
	titleRect.origin.y = cellFrame.origin.y;
	titleRect.size.width = cellFrame.size.width;
	titleRect.size.height = titleSize.height;

	imageRect.origin.x = cellFrame.origin.x;
	imageRect.origin.y = cellFrame.origin.y;
	imageRect.origin.y += titleRect.size.height + yDist;
	imageRect.size.width = cellFrame.size.width;
	imageRect.size.height = cellFrame.size.height;
	imageRect.size.height -= titleSize.height + yDist;

	if (_cell.is_bordered || _cell.is_bezeled) 
	  {
	    imageRect.size.width -= 6;
	    imageRect.origin.x   += 3;
	    titleRect.size.width -= 6;
	    titleRect.origin.x   += 3;
	    imageRect.size.height -= 1;
	    titleRect.size.height -= 1;
	    titleRect.origin.y    += 1;
	  }
	break;

      case NSImageBelow: 
	/*
	 * In this case, imageRect is all the space we can allocate
	 * below the text. 
	 * The drawing code below will then center the image in imageRect.
	 */
	titleRect.origin.x = cellFrame.origin.x;
	titleRect.origin.y = cellFrame.origin.y + cellFrame.size.height;
	titleRect.origin.y -= titleSize.height;
	titleRect.size.width = cellFrame.size.width;
	titleRect.size.height = titleSize.height;

	imageRect.origin.x = cellFrame.origin.x;
	imageRect.origin.y = cellFrame.origin.y;
	imageRect.size.width = cellFrame.size.width;
	imageRect.size.height = cellFrame.size.height;
	imageRect.size.height -= titleSize.height + yDist;

	if (_cell.is_bordered || _cell.is_bezeled) 
	  {
	    imageRect.size.width -= 6;
	    imageRect.origin.x   += 3;
	    titleRect.size.width -= 6;
	    titleRect.origin.x   += 3;
	    imageRect.size.height -= 1;
	    imageRect.origin.y    += 1;
	    titleRect.size.height -= 1;
	  }
	break;

      case NSImageOverlaps: 
	titleRect = cellFrame;
	imageRect = cellFrame;
	// TODO: Add distance from border if needed
	break;
    }
  if (imageToDisplay != nil)
    {
      NSSize size;
      NSPoint position;

      size = [imageToDisplay size];
      position.x = MAX(NSMidX(imageRect) - (size.width/2.),0.);
      position.y = MAX(NSMidY(imageRect) - (size.height/2.),0.);
      /*
       * Images are always drawn with their bottom-left corner at the origin
       * so we must adjust the position to take account of a flipped view.
       */
      if (flippedView)
	{
	  position.y += size.height;
	}
      [imageToDisplay compositeToPoint: position operation: NSCompositeSourceAtop];
    }
  if (titleToDisplay != nil)
    {
      [self _drawText: titleToDisplay inFrame: titleRect];
    }

/*
  if (_cell.shows_first_responder
      && [[controlView window] firstResponder] == controlView)
    {
      if (_cell.is_bordered || _cell.is_bezeled)
	NSDottedFrameRect(cellFrame);
      else
	NSDottedFrameRect(titleRect);
    }
*/
  [controlView unlockFocus];
}

/*
- (void) setButtonType: (NSButtonType)buttonType
{
  // Don't store the button type anywhere

  NSBundle* bundle = [NSBundle bundleForClass: NSClassFromString(@"Theme")];

  switch (buttonType)
    {
      case NSMomentaryLight: 
	[self setHighlightsBy: NSChangeBackgroundCellMask];
	[self setShowsStateBy: NSNoCellMask];
	[self setImageDimsWhenDisabled: YES];
	break;
      case NSMomentaryPushButton: 
	[self setHighlightsBy: NSPushInCellMask | NSChangeGrayCellMask];
	[self setShowsStateBy: NSNoCellMask];
	[self setImageDimsWhenDisabled: YES];
	break;
      case NSMomentaryChangeButton: 
	[self setHighlightsBy: NSContentsCellMask];
	[self setShowsStateBy: NSNoCellMask];
	[self setImageDimsWhenDisabled: YES];
	break;
      case NSPushOnPushOffButton: 
	[self setHighlightsBy: NSPushInCellMask | NSChangeGrayCellMask];
	[self setShowsStateBy: NSChangeBackgroundCellMask];
	[self setImageDimsWhenDisabled: YES];
	break;
      case NSOnOffButton: 
	[self setHighlightsBy: NSChangeBackgroundCellMask];
	[self setShowsStateBy: NSChangeBackgroundCellMask];
	[self setImageDimsWhenDisabled: YES];
	break;
      case NSToggleButton: 
	[self setHighlightsBy: NSPushInCellMask | NSContentsCellMask];
	[self setShowsStateBy: NSContentsCellMask];
	[self setImageDimsWhenDisabled: YES];
	break;
      case NSSwitchButton: 
	[self setHighlightsBy: NSContentsCellMask];
	[self setShowsStateBy: NSContentsCellMask];
	[self setImage: [[NSImage alloc] initWithContentsOfFile: 
	    [bundle pathForImageResource: @"SwitchOff.tiff"]]];
	[self setAlternateImage: [[NSImage alloc] initWithContentsOfFile: 
	    [bundle pathForImageResource: @"SwitchOn.tiff"]]];
	[self setImagePosition: NSImageLeft];
	[self setAlignment: NSLeftTextAlignment];
	[self setBordered: NO];
	[self setBezeled: NO];
	[self setImageDimsWhenDisabled: NO];
	break;
      case NSRadioButton: 
	[self setHighlightsBy: NSContentsCellMask];
	[self setShowsStateBy: NSContentsCellMask];
	[self setImage: [[NSImage alloc] initWithContentsOfFile: 
	    [bundle pathForImageResource: @"RadioOff.tiff"]]];
	[self setAlternateImage: [[NSImage alloc] initWithContentsOfFile: 
	    [bundle pathForImageResource: @"RadioOn.tiff"]]];
	[self setImagePosition: NSImageLeft];
	[self setAlignment: NSLeftTextAlignment];
	[self setBordered: NO];
	[self setBezeled: NO];
	[self setImageDimsWhenDisabled: NO];
	break;
    }
}
*/

@end
